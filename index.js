let inputName = document.getElementById("name")
let inputEmail = document.getElementById("email")
let inputFav = document.getElementById("fav")
let inputColor = document.getElementById("color")
let inputRatting = document.getElementById("rate")
let inputGenre = document.querySelectorAll('.genre')
let inputTerms = document.getElementById("terms")



let form = document.getElementById("form")

let error = document.getElementById("errors")

let bdy = document.querySelector('body')
let popup = document.createElement('div')

document.getElementById('fav').addEventListener('click', () => {
    let rateLab = document.getElementById("rate-lab")
    rateLab.innerText = `Rate the movie ${inputFav.value} out of 10`
})



form.addEventListener('submit', (event) => {

    event.preventDefault()
    createUi()



})
popup.addEventListener('click', (e) => {
    if (e.target.matches('button')) {
        popup.style.display = "none"
        form.style.display = "flex"
        Array.from(popup.children).forEach((ele) => {
            ele.remove()
        })

    }


})

function createUi() {
    let name = inputName.value
    let email = inputEmail.value;
    let fav = inputFav.value;
    let color = inputColor.value;
    let ratting = Number(inputRatting.value) / 10;
    let book = ""
    Array.from(inputGenre).forEach((ele) => {
        if (ele.checked) {
            book = ele.nextElementSibling.innerText;
            ele.checked = false
        }
    })
    if (validate(name, email, fav, color, ratting, book, inputTerms.checked)) {
        createPopup(name, email, fav, color, ratting, book)
    }
}
function createPopup(name, email, fav, color, ratting, book) {
    inputColor.value = "#ffffff"
    inputName.value = ""
    inputEmail.value = ""
    inputFav.value = "Movies"
    inputRatting.value = "50"
    inputTerms.checked = false


    form.style.display = "none"
    popup.style.height = "300px"
    popup.style.width = "450px"
    popup.style.background = "white"
    popup.style.marginRight = "auto"
    popup.style.marginLeft = "auto"
    popup.style.marginTop = "100px"
    popup.style.position = "relative"
    popup.style.padding = "40px"
    popup.style.display = "flex"
    popup.style.flexDirection = "column"
    popup.style.justifyContent = "space-between"

    let closeButton = document.createElement('button')
    closeButton.innerText = "Close"
    closeButton.style.border = "none"
    closeButton.style.position = "absolute"
    closeButton.style.background = "white"
    closeButton.style.top = "10px"
    closeButton.style.right = "10px"
    closeButton.style.cursor = "pointer"
    popup.append(closeButton)


    let heading = createElm("Hello " + name)
    heading.style.fontSize = "34px"
    popup.append(heading)
    popup.append(createElm("Email: " + email))
    popup.append(createElm("You Love: " + fav))
    popup.append(createElm("Color: " + color))
    popup.append(createElm("Rating: " + ratting))
    popup.append(createElm(`Book Genre: ${book} ${color}`))
    let tearmAndCondition = createElm(`👉You agree to Terms and Condition`)
    tearmAndCondition.style.textDecoration = "underline"
    popup.append(tearmAndCondition)
    bdy.append(popup)
}
function createElm(str) {
    let nameElm = document.createElement('p')
    nameElm.innerText = str
    nameElm.style.fontWeight = "600"
    nameElm.style.fontSize = "18px"
    return nameElm
}

function createAndAddError(place, str) {
    place.style.color = 'red'
    place.innerText = str
}
function validate(name, email, fav, color, ratting, book, terms) {
    //name validation
    let flag = true;
    let nameError = document.getElementById('name-error')
    if (name == "" || name == undefined || name == null) {
        createAndAddError(nameError, "* Name should not be Empty")
        flag = false
    } else {
        if (name.length >= 20) {
            createAndAddError(nameError, "* Length of name should not be more than 20")
            flag = false
        } else {
            createAndAddError(nameError, "")
        }
    }



    //email validation
    let emailError = document.getElementById('email-error')
    if (email == "" || email == undefined || email == null) {
        createAndAddError(emailError, "* email should not be Empty")
        flag = false
    } else {
        if (email.length >= 50) {
            createAndAddError(emailError, "* Length of email should not be more than 50")
            flag = false
        } else {
            createAndAddError(emailError, "")
        }
    }

    //movie validation
    let favError = document.getElementById("error-fav")
    if (fav == "Movies") {
        createAndAddError(favError, "* Please select a movie")
        flag = false
    } else {
        createAndAddError(favError, "")
    }
    // book validate
    let bookError = document.getElementById('book-error')
    if (book == "" || book == undefined || book == null) {
        createAndAddError(bookError, "* Please select any one option")
        flag = false
    } else {
        createAndAddError(bookError, "")
    }


    //terms and condition validation
    let termsError = document.getElementById('error-terms')
    if (!terms) {
        alert("please accept the terms and condition")
        createAndAddError(termsError, "* Please accept the terms and condition")
        flag = false
    } else {
        createAndAddError(termsError, "")
    }

    return flag
}